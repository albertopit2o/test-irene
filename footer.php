<?php
/**
 * Template for displaying the footer
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */
?>

    <!-- NEWSLETTER -->
    <!--section class="newsletter container-fluid hidden-xs">
        <div class="container">
            <div class="news-left">
                <div class="news-claim"><img src="<?=get_stylesheet_directory_uri()?>/assets/img/icon_newsletter.png" /> NEWSLETTER</div>
                Suscríbete y recibe todas nuestras novedades en tu email.
            </div>
            <div class="news-right">
                <?php echo do_shortcode( '[contact-form-7 id="50923" title="Newsletter footer"]' ); ?>
            </div>
        </div>
    </section-->

    <!-- FOOTER -->
    <footer class="container-fluid hidden-xs">
        <div class="container">
            <?php
              wp_nav_menu( array(
                'walker' => new Liberty_Footer_Menu_Walker(),
                'theme_location' => 'nav_footer',
                'menu_class'     => 'footer-menu',
                'container'      => 'div',
                'container_class'=> 'first-row',
                'items_wrap'     => '%3$s',
                'fallback_cb'    => 'bootstrap_canvas_wp_menu_fallback',
              ) );
              ?>

            <div class="last-row">
                <a class="modal-politica" data-toggle="modal" data-target="#modal-privacidad" href="#">Política de Privacidad</a>
                <span class="separator">|</span>
                <a class="modal-condiciones" data-toggle="modal" data-target="#modal-condiciones" href="#">Aviso Legal</a>
                <span class="separator">|</span>
                <a class="modal-cookies" data-toggle="modal" data-target="#modal-cookies" href="#">Política de Cookies</a>
            </div>
        </div>
    </footer>
    

    <?php 
	  wp_footer(); 
	?>
  </body>
</html>