var gulp 		= require('gulp'),
	sass 		= require('gulp-sass'),
	sourcemaps  = require('gulp-sourcemaps'),
	watch 		= require('gulp-watch');

//gulp.task('sass', function(){
// return gulp.src('sass/mizona.scss')
// .pipe(sass()) // Converts Sass to CSS
// .pipe(gulp.dest('css'))
//});


gulp.task('sass', function() {
    return gulp.src('sass/mizona.scss')
        .pipe(sourcemaps.init())
            .pipe(sass())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('css'));
});


gulp.task('watch', function(){
 gulp.watch('sass/**/*.scss', ['sass']);
})